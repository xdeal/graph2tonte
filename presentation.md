BENMAMMAR Nabil

DEAL Xavier


**Les graphes appliqués à une tondeuse automatique.**


L'intéret est de montrer une application possible des graphes. Ici le parcours d'un graphe en profondeur permet de déterminer la trajectoire de la tondeuse automatique. En effet
chaque noeud du graphe represente des coordonnées réelles sur le terrain.
On voit graphiquement sur l'image que le parcours DFS permet de tondre la surface de la pelouse.

la recherche d'une application ludique peut être les prémisses d'un TD permettant de mettre en oeuvre différents parcours de graphes (4  ou 8 voisins autour d'un sommet par exemple).
On pourra visualiser l’effet du parcours en profondeur mais aussi en largeur. Mais aussi expérimenter des algorithmes du plus court chemin dans le cas ou la tondeuse est déchargée.
Ce TD peut aussi devenir une base pour des projets de NSI.

Ces notebooks s'apprécient dans l'ordre suivant: 1-notebook_parcoursGraphe puis 2-notebook_CourtChemin.
Récupérable sur [framagit](https://framagit.org/xdeal/graph2tonte)

Que votre exploration soit plaisante et la tonte efficace.
