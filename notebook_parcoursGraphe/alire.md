## notebook parcours de graphe
ce notebook est le premier de 2 notebook consacrés au graphes. Il traite du parcours d'un graphe à travers le cas pratique d'une tondeuse automatique. Il peut se présenter comme un TD devant la classe.
### objectifs
*  Permet de mettre en oeuvre un parcourt BFS et DFS dans le cas d'une tonte de gazon parsemée d'obstacles, sa durée est de 1h30.
*  L'élève peut agir sur le nombre et l'ordre des voisins afin d'expérimenter les parcours de graphes.
*  Appropriation du vocabulaire propre aux graphes.

#### pense-bête et références:

installer le module [opencv-python](https://pypi.org/project/opencv-python/):
pip install opencv-python


https://www.researchgate.net/publication/281884972_Application_du_partitionnement_de_graphes_a_la_segmentation_d%27images



