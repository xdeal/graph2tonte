# exemple permettant de visualiser la tonte d'une pelouse
# installer opencv sur la machine
#https://www.pyimagesearch.com/2018/07/19/opencv-tutorial-a-guide-to-learn-opencv/

from queue import Queue
import cv2

start = (5,5)
end = (80,83)


def getAdjacent(n):
    i,j = n
    return [(i,j+1),(i-1,j+1) , (i-1,j), (i-1,j-1), (i,j-1), (i+1,j-1) ,(i+1,j) ,(i+1,j+1) ]

def BFS(start, end, pixels):
    queue = Queue()
    queue.put([start]) # Wrapping the start tuple in a list
    while not queue.empty():
        path = queue.get() 
        pixel = path[-1]
        if pixel == end:
            return path
        for adjacent in getAdjacent(pixel):
            i,j = adjacent
            if (pixels[i,j] != 0).all():
                pixels[i,j,0] = 250 # colorier avec la couleur BGR 
                pixels[i,j,1] = 250
                pixels[i,j,2] = 0
                new_path = list(path)
                new_path.append(adjacent)
                queue.put(new_path)
                #commenter les lignes suivantes pour la rapidité
                cv2.imshow("tonte", pixels)
                cv2.waitKey(1)

    print( "Pas de chemin trouvé.")


if __name__ == "__main__":
    cv2.namedWindow("tonte", cv2.WINDOW_NORMAL)
    cv2.resizeWindow('tonte', 800, 600)#largeur,hauteur
    img = cv2.imread(r"jardin.pnm")# 90*120 pixels
    height, width,depth = img.shape[0], img.shape[1], img.shape[2]
    print("height: ",height," width: ",width," depth: ",depth)
    chemin = BFS(start, end, img)#i->y->lignes->height et j->x->colonnes->width
    print("taille: ",len(chemin))
    for position in chemin:
        i,j = position
        img[i,j,0] = 0 # colorier avec la couleur BGR 
        img[i,j,1] = 0
        img[i,j,2] = 250   
    cv2.imshow("tonte", img)
    cv2.waitKey(0)#attend appui touche
    cv2.destroyAllWindows()
