# exemple permettant de visualiser la tonte d'une pelouse
# grâce à un parcours en profondeur
# installer opencv sur la machine
#https://www.pyimagesearch.com/2018/07/19/opencv-tutorial-a-guide-to-learn-opencv/

import cv2
# img est un tableau de pixels, i,j les coordonnées de départ suivit de la taille de l'image
def bfs(img,i,j,height,width):
    seen = []
    todo = [(i,j)]
    while todo:
        i,j = todo.pop(0)  #Implement DFS : use .pop()
        if (0 <= i < height) and (0 <= j < width) and (img[i,j] != 0).all() and ((i,j) not in seen):
            cv2.imshow("tonte", img)
            cv2.waitKey(1)
            img[i,j,0] = 0 # colorier avec la couleur BGR 0,0,125
            img[i,j,1] = 250
            img[i,j,2] = 0
            seen.append((i,j))
            todo += [(i,j+1),(i+1,j)] 
    return seen 


if __name__ == "__main__":
    cv2.namedWindow("tonte", cv2.WINDOW_NORMAL)
    cv2.resizeWindow('tonte', 800, 600)#largeur,hauteur
    img = cv2.imread(r"jardin.pnm")# 90*120 pixels
    height, width,depth = img.shape[0], img.shape[1], img.shape[2]
    print("height: ",height," width: ",width," depth: ",depth)
    i,j = 20,20 # coordonnées de départ
    fait = bfs(img, i, j, height,width)#i->y->lignes->height et j->x->colonnes->width
    print("taille: ",len(fait))
    cv2.imshow("tonte", img)
    cv2.waitKey(0)#attend appui touche
    cv2.destroyAllWindows()
