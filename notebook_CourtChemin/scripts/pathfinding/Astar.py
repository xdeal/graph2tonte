# exemple permettant de visualiser la tonte d'une pelouse
# grâce à un parcours en profondeur
# installer opencv sur la machine
#https://www.pyimagesearch.com/2018/07/19/opencv-tutorial-a-guide-to-learn-opencv/

from pathfinding.core.diagonal_movement import DiagonalMovement
from pathfinding.core.grid import Grid
from pathfinding.finder.a_star import AStarFinder


import cv2



if __name__ == "__main__":
    cv2.namedWindow("tonte", cv2.WINDOW_NORMAL)
    cv2.resizeWindow('tonte', 800, 600)#largeur,hauteur
    img = cv2.imread(r"jardin.pnm",0)# 90*120 pixels niveau de gris
    ret, img = cv2.threshold(img, 10, 255, cv2.THRESH_BINARY) # image noire et blanc
    # height, width,depth = img.shape[0], img.shape[1], img.shape[2]
    # print("height: ",height," width: ",width," depth: ",depth)
    #chemin = BFS(start, end, img)#i->y->lignes->height et j->x->colonnes->width
    # print("taille: ",len(chemin))
    # for position in chemin:
    #     i,j = position
    #     img[i,j,0] = 0 # colorier avec la couleur BGR 0,0,125
    #     img[i,j,1] = 0
    #     img[i,j,2] = 250   
    grid = Grid(matrix=img)
    print(grid.height, grid.width)
    start = grid.node(5, 5)
    end = grid.node(80, 83)
  
    #finder = AStarFinder(diagonal_movement=DiagonalMovement.always)
    finder = AStarFinder()
    path, runs = finder.find_path(start, end, grid)
    print('operations:', runs, 'path length:', len(path))
    for position in path:
        i,j = position
        img[i,j] = 125 # colorier avec la couleur BGR 0,0,125
        # img[i,j,1] = 0
        # img[i,j,2] = 250 
    
    cv2.imshow("tonte", img)
    cv2.waitKey(0)#attend appui touche
    cv2.destroyAllWindows()
