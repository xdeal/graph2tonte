# exemple permettant de visualiser la tonte d'une pelouse
# installer opencv sur la machine
#https://www.pyimagesearch.com/2018/07/19/opencv-tutorial-a-guide-to-learn-opencv/


#import Bfs4voisinsCourtChemin as cc
import Bfs8voisinsCourtChemin as cc
import cv2
import time

base = 20,20# coordonnées de la base de ravitaillement
i,j = base
start = (5,5)
end = (80,83)

def ravitaillement(i,j):
    print("Demande de ravitaillement!")
    start = i,j #coordonnées de la tondeuse lors de la demande de ravitaillement          
    end = base # coordonnées de la base
    print("coordonnees i: ",i," j: ",j,"end", end," start: ",start)
    img2 = cv2.imread(r"jardin.pnm")
    chemin = cc.BFS(start,end,img2)
    for position in chemin:
        i,j = position
        img[i,j,0] = 0 # colorier avec la couleur BGR 
        img[i,j,1] = 250
        img[i,j,2] = 250 
    cv2.imshow("tonte", img)
    cv2.waitKey(1)
    time.sleep(2)#animation ralentie
    #retour tonte
    print("retour aux coordonnées de tonte")
    end = start 
    start = base
    print("coordonnees i: ",i," j: ",j,"end", end," start: ",start)
    img3 = cv2.imread(r"jardin.pnm")
    chemin = cc.BFS(start,end,img3)
    for position in chemin:
        i,j = position
        img[i,j,0] = 250 # colorier avec la couleur BGR 
        img[i,j,1] = 0
        img[i,j,2] = 250
    cv2.imshow("tonte", img)
    cv2.waitKey(1)
    time.sleep(2)#animation ralentie
    print("Reprise du cycle normal de tonte!")
    #reprise normale

# img est un tableau de pixels, i,j les coordonnées de départ suivit de la taille de l'image
def dfs(img,i,j,height,width):
    seen = []
    todo = [(i,j)]
    while todo:
        i,j = todo.pop()  #Implement DFS : use .pop()
        if (0 <= i < height) and (0 <= j < width) and (img[i,j] != 0).all() and ((i,j) not in seen):
            cv2.imshow("tonte", img)
            k = cv2.waitKey(1)
            if k == 27: #touche escape
                ravitaillement(i,j)
                
            img[i,j,0] = 0 # colorier avec la couleur BGR 0,0,125
            img[i,j,1] = 250
            img[i,j,2] = 0
            seen.append((i,j))
            todo += [(i,j+1),(i+1,j+1),(i+1,j), (i+1,j-1), (i,j-1) , (i-1,j-1)  , (i-1,j),(i-1,j+1)] 
    return seen 


if __name__ == "__main__":
    cv2.namedWindow("tonte", cv2.WINDOW_NORMAL)
    cv2.resizeWindow('tonte', 800, 600)#largeur,hauteur
    img = cv2.imread(r"jardin.pnm")# 90*120 pixels
    height, width,depth = img.shape[0], img.shape[1], img.shape[2]
    print("height: ",height," width: ",width," depth: ",depth)   
    fait = dfs(img, i, j, height,width)#i->y->lignes->height et j->x->colonnes->width
    print("taille: ",len(fait))
    cv2.imshow("tonte", img)
    cv2.waitKey(0)#attend appui touche
    cv2.destroyAllWindows()
