## notebook le plus court chemin
ce notebook fait suite au notebook parcourt d'un graphe et peut se conduire en classe comme un TD
### objectifs
*  Permet de mettre en oeuvre un parcourt BFS dans le cas d'une recherche du plus court chemin
sa durée est de 1h30.
*  La partie recharge de la batterie demande plus de codage donc la durée est estimée à 2h30.
*  La partie sur la découverte d'un module de pathfinding peut se faire en 1h et permet d'allez un peu plus loin.

pense-bête et références:

installer le module [opencv-python](https://pypi.org/project/opencv-python/):
pip install opencv-python

installer le module [pathfinding](https://pypi.org/project/pathfinding/):
pip install pathfinding

https://www.youtube.com/watch?time_continue=9&v=pVfj6mxhdMw&feature=emb_logo
https://stackoverflow.com/questions/12995434/representing-and-solving-a-maze-given-an-image/13174351#13174351


